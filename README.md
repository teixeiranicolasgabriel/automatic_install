# 1.  Automatic Install

Uma formatação de PC, pode ser bem chata por causa do pós e a instalação de cada software que você precisa usar. Essa etapa de preparação é bem maçante e chata. Então que tal, automatizar essa parte ?! 

Foi pensando nisso que esse projeto foi desenvolvido. Tirar essa dor que muitas vezes impossibilita a gente a testar uma distro nova.

# 2. Requisitos

<ul>
<li> Ubuntu distro or Similar (Ex: Mint)
<li> Python
</ul>

# 3. O que o projeto instala

Segue a lista de componentes que o projeto instala na atual versão:

## 3.1. Tools

<ul>
   <li> curl
   <li> apt-transport-https
   <li> ca-certificates
   <li> software-properties-common
   <li> lvm2
   <li> python-pip
   <li> python-setuptools
   <li> python3-setuptools
   <li> nautilus
</ul>

##  3.2. Softwares
<ul>
  <li> Docker
  <li> Docker-Compose
  <li> Git
  <li> Audacity
  <li> Spotify
  <li> Visual Studio Code
  <li> KeePass2
</ul>


# 3. SETUP
<p>O projeto tem um arquivo de configuração, que é pra setar algumas váriaveis que serão usadas pelo playbook. Abaixo o caminho do arquivo:</p>

    automatic_install/playbook/group_vars/all.yml
    
# 4. Executar o script

<p>Depois de configurado o arquivo <b><i>all.yml</i></b>. Execute os passos abaixo:</p>

<ul>
<li> Entrar na raiz do projeto
</ul>

    cd automatic_install/
   
<ul>
<li> Execute o script <b><i>install.sh</i></b>
</ul>

    sudo sh install.sh
    
<p>Pronto... Daqui alguns minutos seu computador está pronto pra uso com seus softwares.</p>

# 5. Proximos Passos

<ul>
<li> Criar um video explicando cada passo do script
<li> Fazer Branch para distros baseadas em Debian
<li> Configurar mais softwares no playbook*
<li> Configurar meus projetos pessoais do git lab no computador
<li> Baixar o database do KeePass do meu google drive
<li> Terminar uma esteira de CI/CD aqui no GitLab.
<li> Quitar as dividas tecnicas iniciais.
</ul>

<b>OBS:</b> O terceiro ponto é mais um tópico constante, cada vez que um software ficar necessário no meu dia a dia, será preciso configurar no playbook, e o contrário é verdadeiro, a partir do momento que eu não use mais determinado software, é preciso retira-lo do playbook

# 6. Considerações

* Esse projeto esta configurado para as minhas necessidades. Se você jovem guerreiro(a) desse mundo linux, quiser personalizar um para você, manda uma mensagem que a gente conversa e cria... Sem crise.

* Com essa automação, meu tempo de preparação da máquina passou de 5 horas praticamente, pra 10 á 15 minutos, dependendo da conexão com a internet.


## 6.1. Contatos

<p>Linkedin: https://www.linkedin.com/in/nicolas-gabriel-teixeira-512533158</p>
<p>Email: teixeiranicolasgabriel@gmail.com</p>
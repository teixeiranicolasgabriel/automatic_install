#!/bin/bash
install_ansible(){
    echo "Update System Configs"
    sudo apt update
    echo "Install Dependencies"
    sudo apt install software-properties-common
    echo "Config Repository"
    sudo apt-add-repository --yes --update ppa:ansible/ansible
    echo "Install Ansible"
    sudo apt install ansible -y
}

# Set Playbook Path
playbookPATH="$HOME/automatic_install/playbook"

# Instalar Playbook
install_ansible

# Roda Playbook
ansible-playbook -i $playbookPATH/hosts $playbookPATH/provisioning.yml

echo -e "#########################"
echo -e "[INFO] INSTALL COMPLETED"
echo -e "#########################"
echo -e "THAT'S ALL FOLKS"
